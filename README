A simple command line interface for Contegix Miracloud.

https://bitbucket.org/wikistrat/miracloud-cli

This is intended to provide a simple interface for the Miracloud virtual machines.

Features:
  * Start, stop, reboot and query virtual machines

Dependencies:
  * Cement 2.0 http://builtoncement.com/2.0/

Configuration
-------------

Add your application key and application secret to miracloud.conf


Usage
-----

optional arguments:
  -h, --help      show this help message and exit
  --debug         toggle debug output
  --quiet         suppress all output
  --start         start vm
  --stop          stop vm
  --reboot        reboot vm
  -s, --state     display vm state
  -l, --list      list state of all vm
  -v VM, --vm VM  name or id of vm

To start a virtual machine named, 'mailserver1' you would run:
``python miracloud.py -v mailserver1 --start``


Sample Output:

List all virtual machines
``$ python miracloud.py -l``

::

    miracloud: listing virtual machines

    Alias:  mailserver1
    Name:   i-11327-44212-VM
    ID:     26343
    Domain: AA015515
    State:  Running

    Alias:  mailserver2
    Name:   i-11537-16375-VM
    ID:     13255
    Domain: AA015515
    State:  Stopped

Stop a virtual machine and display it's state
``$ python miracloud.py -v mailserver1 --stop -s``

::

    miracloud: stopping virtual machine
    virtual machine "mailserver1" is stopping.
    miracloud: retrieving virtual machine state

    Alias:  mailserver1
    Name:   i-11327-44212-VM
    ID:     26343
    Domain: AA015515
    State:  Stopping

Alternate syntax using ID instead of name and skipping state
``$ python miracloud.py -v 26343 --stop``

::

    miracloud: stopping virtual machine
    virtual machine #26343 is stopping.