#!/usr/bin/python

"""miracloud.py: Contegix Miracloud Command Line Interface."""

# Import URL and HTTP libraries
import urllib, urllib2, httplib, json
# Import encryption and encoding
import hmac, base64, hashlib
# Import system functinos
import sys
# Import Cement Framework
from cement.core import foundation

# Calculate the request signature as per API specification
def calculate_signature (secret_key, command_string):
    "calculate signature of API call"
    return urllib.quote_plus(base64.b64encode(hmac.new(secret_key,command_string.lower(),hashlib.sha1).digest()))

# Build the API request
def build_request (secret_key, api_params):
    "build the API request"
    request_string = '' # Initialize string
    keylist = api_params.keys()
    keylist.sort()
    for key in keylist:
        request_string += key + '=' + api_params[key] + '&'
    request_string = request_string[:-1]
    return request_string + '&signature=' + calculate_signature(secret_key, request_string)

# Make an API call
def api_call (base_url, request_string):
    try:
        return urllib2.urlopen(base_url + '?' + request_string).read()
    except urllib2.HTTPError, e:
        print "HTTP error: %d" % e.code
    except urllib2.URLError, e:
        print "Network error: %s" % e.reason.args[1]
    except httplib.BadStatusLine, e:
        print "Something went wrong!"


# Call the Cement CLI Framework
app = foundation.CementApp('miracloud')

try:
    app.setup() # Cement setup section

    # Load and parse config files
    app.config.parse_file('miracloud.conf')
    api_key = app.config.get('keys', 'api')
    secret_key = app.config.get('keys', 'secret')
    base_url = app.config.get('server', 'baseurl')

    # Add available command line options
    app.args.add_argument('-v', '--vm', action='store', dest='vm',
        help='name or id of vm ')
    app.args.add_argument('--start', action='store_true', dest='start',
        help='start vm')
    app.args.add_argument('--stop', action='store_true', dest='stop',
        help='stop vm')
    app.args.add_argument('--reboot', action='store_true', dest='reboot',
        help='reboot vm')
    app.args.add_argument('-s', '--state', action='store_true', dest='info',
        help='display vm state')
    app.args.add_argument('-l', '--list', action='store_true', dest='list',
        help='list state of all vm')

    # Start the application
    app.run()

    # Compulsory parameters
    api_params = {'apiKey': api_key, 'response': 'json'}

    # Primary application flow
    if app.pargs.stop:
        if not app.pargs.vm:
            sys.exit('error: no virtual machine specified')
        api_params['command'] = 'stopVirtualMachine'
        print 'miracloud: stopping virtual machine'

        if app.pargs.vm.isdigit():
            print "virtual machine #"+app.pargs.vm+" is stopping."
            api_params['id'] = app.pargs.vm
            api_call (base_url, build_request(secret_key, api_params))
        else:
            api_params['command'] = 'listVirtualMachines'
        json_response = json.loads(api_call (base_url, build_request(secret_key, api_params)))

        for vm in json_response['listvirtualmachinesresponse']['virtualmachine']:
            if vm['displayname'] == app.pargs.vm:
                print "virtual machine \""+vm['displayname']+"\" is stopping."
                api_params['command'] = 'stopVirtualMachine'
                api_params['id'] = str(vm['id'])
                api_call (base_url, build_request(secret_key, api_params))

    #TODO: Move repetitive logic into functions

    elif app.pargs.start:
        if not app.pargs.vm:
            sys.exit('error: no virtual machine specified')
        api_params['command'] = 'startVirtualMachine'
        print 'miracloud: starting virtual machine'

        if app.pargs.vm.isdigit():
            print "virtual machine #"+app.pargs.vm+" is starting."
            api_params['id'] = app.pargs.vm
            api_call (base_url, build_request(secret_key, api_params))
        else:
            api_params['command'] = 'listVirtualMachines'
        json_response = json.loads(api_call (base_url, build_request(secret_key, api_params)))

        for vm in json_response['listvirtualmachinesresponse']['virtualmachine']:
            if vm['displayname'] == app.pargs.vm:
                print "virtual machine \""+vm['displayname']+"\" is starting."
                api_params['command'] = 'startVirtualMachine'
                api_params['id'] = str(vm['id'])
                api_call (base_url, build_request(secret_key, api_params))

    elif app.pargs.reboot:
        if not app.pargs.vm:
            sys.exit('error: no virtual machine specified')
        api_params['command'] = 'rebootVirtualMachine'
        print 'miracloud: rebooting virtual machine'

        if app.pargs.vm.isdigit():
            print "virtual machine #"+app.pargs.vm+" is rebooting."
            api_params['id'] = app.pargs.vm
            api_call (base_url, build_request(secret_key, api_params))
        else:
            api_params['command'] = 'listVirtualMachines'
        json_response = json.loads(api_call (base_url, build_request(secret_key, api_params)))

        for vm in json_response['listvirtualmachinesresponse']['virtualmachine']:
            if vm['displayname'] == app.pargs.vm:
                print "virtual machine \""+vm['displayname']+"\" is rebooting."
                api_params['command'] = 'rebootVirtualMachine'
                api_params['id'] = str(vm['id'])
                api_call (base_url, build_request(secret_key, api_params))

    # Don't need this parameter after this point
    if 'id' in api_params.keys():
        del api_params['id']

    if app.pargs.info:
        # List all virtual machines
        api_params['command'] = 'listVirtualMachines'
        json_response = json.loads(api_call (base_url, build_request(secret_key, api_params)))

        print 'miracloud: retrieving virtual machine state'

        for vm in json_response['listvirtualmachinesresponse']['virtualmachine']:
            if app.pargs.vm.isdigit() and str(app.pargs.vm) == str(vm['id']):
                print ''
                print "Alias:  " + vm['displayname']
                print "Name:   " + vm['name']
                print "ID:     " + str(vm['id'])
                print "Domain: " + vm['domain']
                print "State:  " + vm['state']

            if not app.pargs.vm.isdigit() and app.pargs.vm == vm['displayname']:
                print ''
                print "Alias:  " + vm['displayname']
                print "Name:   " + vm['name']
                print "ID:     " + str(vm['id'])
                print "Domain: " + vm['domain']
                print "State:  " + vm['state']

    if app.pargs.list:
        # List all virtual machines
        api_params['command'] = 'listVirtualMachines'

        json_response = json.loads(api_call (base_url, build_request(secret_key, api_params)))

        print 'miracloud: listing virtual machines'

        for vm in json_response['listvirtualmachinesresponse']['virtualmachine']:
            print ''
            print "Alias:  " + vm['displayname']
            print "Name:   " + vm['name']
            print "ID:     " + str(vm['id'])
            print "Domain: " + vm['domain']
            print "State:  " + vm['state']

finally:
    app.close()

